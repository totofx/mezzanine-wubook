from django.utils import timezone
from django.utils.translation import ugettext as _
from django import forms
from mezzanine.conf import settings


class WubookForm(forms.Form):
    dfrom = forms.DateField(
        label=_('Arrival date'),
        initial=timezone.now,
        input_formats=['%d/%m/%Y', ]
    )

    dto = forms.DateField(
        label=_('Departure date'),
        initial=timezone.now,
        input_formats=['%d/%m/%Y', ]
    )

    # nights = forms.ChoiceField(
    #     choices=(
    #         (1, 1),
    #     ),
    #     label=_('Nights'),
    #     widget=forms.Select(
    #         attrs={
    #             'class': 'form-control'
    #         }
    #     )
    # )

    # def __init__(self, *args, **kwargs):
    #
    #     super(WubookForm, self).__init__(*args, **kwargs)
    #     settings.use_editable()
    #
    #     try:
    #         max_nights = int(settings.WUBOOK_MAX_NIGHTS) + 1
    #
    #     except ValueError:
    #         max_nights = 30
    #
    #     choices_nights = [(i, i) for i in range(1, max_nights)]
    #     self.fields['nights'].choices = choices_nights

    def get_action(self):
        settings.use_editable()
        return settings.WUBOOK_ACTION_FORM




