from django import template
from django.template.loader import render_to_string
from mezzanine_wubook.forms import WubookForm

register = template.Library()

@register.assignment_tag(takes_context=True)
def get_wubook_form(context):
    request = context['request']

    if request.GET.get('dfrom') and request.GET.get('dto'):
        form = WubookForm(request.GET)

    else:
        form = WubookForm()

    return form

@register.simple_tag
def get_wubook_static_files(type_static):

    if type_static == 'js':
        return render_to_string('mezzanine_wubook/statics/js.html')

    if type_static == 'css':
        return render_to_string('mezzanine_wubook/statics/css.html')

    raise Exception('use css or js parameters only')